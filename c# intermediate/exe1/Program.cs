﻿using System;
using System.Diagnostics;

namespace exe1
{
    class Program
    {
        static void Main(string[] args)
        {
           /* var sw = new StopWatch();
            sw.Start();*/

            var StopWatch = new Stopwatch();
            StopWatch.Start();
            Console.ReadLine();
            StopWatch.Stop();
            TimeSpan elapsed = StopWatch.Elapsed;
            Console.WriteLine( string.Format("{0:00}:{1:00}:{2:00}:{3:00}", Math.Floor(elapsed.TotalHours), elapsed.Minutes, elapsed.Seconds, elapsed.Milliseconds));

            Console.ReadLine();
            Console.WriteLine(DateTime.Now);
        }
    }
}
