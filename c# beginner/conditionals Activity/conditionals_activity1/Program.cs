﻿using System;

namespace conditionals_activity1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please Enter a Number From 1 - 10");
            var number = Convert.ToInt32(Console.ReadLine());
            var output = "Number "+number+" is ";
            if (number >= 1 && number <= 10)
            {
                output += "valid";
            }
            else
            {
                output += "valid";
            }
            Console.WriteLine(output);
        }
    }
}
