﻿using System;

namespace exercise5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter commoa separated numbers: ");

            var numbers = Console.ReadLine().Split(',');

            var max = Convert.ToInt32(numbers[0]);

            foreach (var str in numbers)
            {
                var number = Convert.ToInt32(str);
                if (number > max)
                    max = number;
            }

            Console.WriteLine("Max is " + max);
        }
    }
}
