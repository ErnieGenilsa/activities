﻿using System;

namespace exercise3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a number: ");
            var number = Convert.ToInt32(Console.ReadLine());

            var factorial = 1;

            for (var i = number; i > 0; i--)
            {
                Console.WriteLine(i);
                factorial *= i;
            }


            Console.WriteLine("{0}! = {1}", number, factorial);
        }
    }
}
