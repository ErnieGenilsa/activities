﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace exe4
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<int>();

            while (true)
            {
                Console.Write("Enter a number (or 'Quit' to exit): ");
                var input = Console.ReadLine();

                if (input.ToLower() == "quit")
                    break;
                if (String.IsNullOrEmpty(input))
                {
                    continue;
                }
                var number = Convert.ToInt32(input);
                if (numbers.Contains(number))
                    continue;

                numbers.Add(number);
            }

            Console.WriteLine("Unique numbers are :");
            foreach (var number in numbers)
                Console.WriteLine(number);
        }
    }
}
