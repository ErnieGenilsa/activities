﻿using System;
using System.Collections.Generic;

namespace exe3
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<int>();

            while (numbers.Count < 5)
            {
                Console.Write("Enter a number: ");
                var v = Console.ReadLine();
                // if the user center w/out typing any character
                if (String.IsNullOrEmpty(v))
                {
                    Console.WriteLine("Please enter a Number");
                    continue;
                }
                var number = Convert.ToInt32(v);
                if (numbers.Contains(number))
                {
                    Console.WriteLine("You've previously entered " + number);
                    continue;
                }
                

                numbers.Add(number);
            }

            numbers.Sort();

            foreach (var number in numbers)
                Console.WriteLine(number);
        }
    }
}
