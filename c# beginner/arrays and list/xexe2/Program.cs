﻿using System;

namespace xexe2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("What's your name? ");
            var name = Console.ReadLine();

            char[] charArray = name.ToCharArray();
            Array.Reverse(charArray);
            Console.WriteLine("Reversed name: " + new string(charArray));
        }
    }


}
