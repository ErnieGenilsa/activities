﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Qoutes : ContentPage
    {
        private int _index = 0;
        private string[] _quotes = new string[]
        {
            "Ernie",
            "Nerisa",
            "Dale",
            "Kim"
        };
        public Qoutes()
        {
            InitializeComponent();
            currentQuote.Text = _quotes[_index];
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            _index++;
            if (_index >= _quotes.Length)
            {
                _index = 0;
            }

            currentQuote.Text = _quotes[_index];
        }
    }
}